const image = {
  coverApp: require('./logo.png'),
  logo: require('App/Assets/image/icon.png'),
  search: require('App/Assets/image/search.jpg'),
  books: require('App/Assets/image/books.png'),
  manual: require('App/Assets/image/manual.png'),
  feedback: require('App/Assets/image/feedback.png'),
  question: require('App/Assets/image/question.png'),
  banner: {
    satu: require('App/Assets/image/banner/satu.png'),
    dua: require('App/Assets/image/banner/dua.png'),
    tiga: require('App/Assets/image/banner/tiga.png'),
  },
  cover: {
    satu: require('App/Assets/image/item_list/satu.png'),
    dua: require('App/Assets/image/item_list/tiga.png'),
    tiga: require('App/Assets/image/item_list/dua.png'),
  },
};

export default image;
