import React, {Component} from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
import assets from 'App/Assets';

const NotFound = () => {
  return (
    <View style={style.container}>
      <View style={style.imageSearch}>
        <Image
          source={assets.search}
          style={{width: '100%'}}
          resizeMode="center"
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  imageSearch: {
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
});

export default NotFound;
