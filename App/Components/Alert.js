import React from 'react';
import {View} from 'react-native';
import {Button, Paragraph, Dialog, Portal} from 'react-native-paper';
import {withNavigation} from 'react-navigation';

const Alert = props => {
  return (
    <Portal style={{zIndex: 10000}}>
      <Dialog visible={props.dialog} onDismiss={props.onDismiss}>
        <Dialog.Title>Info !</Dialog.Title>
        <Dialog.Content>
          <Paragraph>
            {props.message
              ? props.message
              : 'Apakah anda ingin menghapus item ini?'}
          </Paragraph>
        </Dialog.Content>
        <Dialog.Actions>
          {props.message ? (
            <Button
              onPress={() => {
                props.callbackDismis();
              }}>
              Oke
            </Button>
          ) : (
            // eslint-disable-next-line react-native/no-inline-styles
            <View style={{flexDirection: 'row'}}>
              <Button onPress={() => props.hapusFile()}>Hapus</Button>
              <Button onPress={() => props.simpan()}>Simpan</Button>
            </View>
          )}
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};
export default withNavigation(Alert);
