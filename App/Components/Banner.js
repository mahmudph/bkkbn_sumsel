import React, {Component} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {
  StyleSheet,
  View,
  TouchableNativeFeedback,
  Dimensions,
  Image,
} from 'react-native';

const {width, height} = Dimensions.get('screen');
const Benner = props => {
  return (
    <TouchableNativeFeedback onPress={props.onPress} key={props.id}>
      <View style={[style.content, props.style ? props.style : {}]}>
        <Image source={props.image} style={style.image} />
      </View>
    </TouchableNativeFeedback>
  );
};

export default Benner;

const style = StyleSheet.create({
  content: {
    flex: 1,
    marginVertical: 5,
    marginHorizontal: 10,
    justifyContent: 'flex-start',
    width: width / 1.5,
    height: height * 0.25,
  },
  image: {
    resizeMode: 'cover',
    borderRadius: 10,
    width: width / 1.5,
    flex: 1,
    height: height * 0.3,
  },
});
