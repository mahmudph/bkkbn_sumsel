/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  Text,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
} from 'react-native';

import {withNavigation} from 'react-navigation';

const {height, width} = Dimensions.get('screen');
const Card = props => {
  return (
    <View style={[style.container, props.style]}>
      <View style={style.contentBase}>
        <View style={style.imageContent}>
          <Image
            source={props.image}
            style={style.images}
            resizeMode="center"
          />
        </View>
        <View style={style.content}>
          <TouchableWithoutFeedback onPress={() => props.openPdf()}>
            <View>
              <Text style={style.caption}>{props.id_module}</Text>
              <Text style={style.title}>Judul : {props.title}</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
      <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 10,
          }}>
          {props.data.ppt ? (
            <TouchableNativeFeedback onPress={() => props.openPdf()}>
              <View style={style.button}>
                <Text style={style.button}>BACA MATERI</Text>
              </View>
            </TouchableNativeFeedback>
          ) : null}
          {props.data.videoUrl ? (
            <TouchableNativeFeedback onPress={() => props.openVideo()}>
              <View style={style.button}>
                <Text style={style.button}>BUKA VIDEO</Text>
              </View>
            </TouchableNativeFeedback>
          ) : null}
        </View>
      </View>
    </View>
  );
};
export default withNavigation(Card);

const style = StyleSheet.create({
  container: {
    backgroundColor: '#31467D',
  },
  contentBase: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 15,
    width: width * 0.96,
    height: height * 0.2,
  },
  content: {
    width: width * 0.63,
    justifyContent: 'center',
  },
  title: {
    fontWeight: '700',
    color: '#eee',
    flexWrap: 'wrap',
    lineHeight: 22,
    fontSize: 14,
    textTransform: 'capitalize',
  },
  caption: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    lineHeight: 30,
    textTransform: 'uppercase',
  },
  images: {
    width: width * 0.25,
    height: height * 0.25,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  imageContent: {
    justifyContent: 'center',
    width: width * 0.33,
  },
  button: {
    borderRadius: 50,
    padding: 5,
    fontWeight: 'bold',
    color: '#FFF',
  },
  love: {
    justifyContent: 'flex-end',
    padding: 10,
    paddingRight: 20,
  },
});
