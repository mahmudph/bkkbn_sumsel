import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Image,
  TouchableNativeFeedback,
} from 'react-native';
import {withNavigation} from 'react-navigation';

const {width, height} = Dimensions.get('screen');
const Grid = props => {
  const openModule = () => {
    if (Array.isArray(props.data.item) && props.data.item.length > 0) {
      props.navigation.navigate('ListPertemuan', {
        data: props.data.item,
        key: props.id_module,
      });
    } else {
      props.navigation.navigate('Content', {
        data: props.data,
        id_module: props.id_module,
        key: props.id_module,
      });
    }
  };
  const ListPertemuan = () => {
    if (props.index === 0) {
      return 'KONSEP ' + `${props.id_module.split(' ').pop()}`;
    } else if (
      props.index === 13 &&
      props.id_module
        .toUpperCase()
        .split(' ')
        .pop() == 'BKR'
    ) {
      return 'RR BKR';
    } else if (
      props.index === 14 &&
      props.id_module
        .toUpperCase()
        .split(' ')
        .pop() == 'BKB'
    ) {
      return 'KKA';
    } else if (
      props.index === 15 &&
      props.id_module
        .toUpperCase()
        .split(' ')
        .pop() == 'BKB'
    ) {
      return 'RR BKB';
    } else if (
      props.index === 14 &&
      props.id_module
        .toUpperCase()
        .split(' ')
        .pop() == 'BKL'
    ) {
      return 'RR BKL';
    } else {
      return props.index;
    }
  };
  return (
    <TouchableNativeFeedback key={props.key} onPressIn={() => openModule()}>
      <View style={[style.content, props.style]}>
        <Text
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            fontSize: 12,
            backgroundColor: '#dddd',
            color: '#000',
            padding: 10,
            fontWeight: 'bold',
            position: 'absolute',
            textTransform: 'uppercase',
            top: 0,
            left: 0,
            zIndex: 100,
          }}>
          {ListPertemuan()}
        </Text>
        {props.image ? (
          <Image
            source={props.image}
            // eslint-disable-next-line react-native/no-inline-styles
            style={[
              style.content,
              // eslint-disable-next-line react-native/no-inline-styles
              {
                marginVertical: 0,
                opacity: 0.9,
                width: '100%',
                height: height / 4.8,
              },
            ]}
            resizeMode="stretch"
          />
        ) : (
          <View
            style={[
              style.content,
              {height: height / 4.8, alignItems: 'center'},
            ]}>
            {props.children}
          </View>
        )}
      </View>
    </TouchableNativeFeedback>
  );
};

export default withNavigation(Grid);

const style = StyleSheet.create({
  content: {
    flexDirection: 'row',
    backgroundColor: 'rgba(49, 70, 125, 0.9)',
    marginVertical: 1,
    marginHorizontal: 0.5,
    width: '49.5%',
  },
});
