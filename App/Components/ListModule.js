import React, {Component} from 'react';
import {Text} from '@ui-kitten/components';
import {
  StyleSheet,
  Image,
  View,
  Dimensions,
  TouchableNativeFeedback,
} from 'react-native';
import {withNavigation} from 'react-navigation';
import {withStyles} from '@ui-kitten/components';

const ListModule = props => {
  const redirectToListModule = () => {
    console.log(props.id, 'hehehe');
    props.navigation.navigate('ListModules', {
      title: props.title,
      data: props.data,
      key: props.id,
    });
  };

  return (
    <View style={[style.container, props.style]} key={props.id}>
      <TouchableNativeFeedback
        key={props.id}
        onPress={() => {
          if (props.favorite) {
            return props.aksi(props.data);
          } else {
            return redirectToListModule();
          }
        }}>
        <View style={[style.content, props.style ? props.style : {}]}>
          <View
            style={[
              style.imageContent,
              props.image ? {} : {alignItems: 'center'},
            ]}>
            {props.image ? (
              <Image
                source={props.image}
                style={style.image}
                resizeMode="cover"
              />
            ) : (
              <View
                style={[
                  style.captions,
                  props.color
                    ? {backgroundColor: props.color}
                    : {
                        backgroundColor: props.theme.color[props.index]
                          ? props.theme.color[props.index]
                          : props.theme.color[0],
                      },
                ]}>
                <Text
                  // eslint-disable-next-line react-native/no-inline-styles
                  style={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    color: '#FFF',
                    fontSize: 28,
                    padding: 10,
                  }}>
                  {props.title ? props.title.charAt(0).toUpperCase() : props.id}
                </Text>
              </View>
            )}
          </View>
          <View style={style.caption}>
            <Text style={style.title}>
              {props.spliteLengthTitle
                ? props.title
                    .split(' ')
                    .slice(0, props.spliteLengthTitle)
                    .join(' ') + '..'
                : props.title}
            </Text>
            {props.jml ? (
              <Text
                style={[style.subtitle, {fontWeight: '500', color: '#2D2D2D'}]}>
                {props.jml} data
              </Text>
            ) : null}
            {props.keys ? (
              <Text
                style={[
                  style.subtitle,
                  {fontWeight: '400', fontSize: 12, color: '#2D2D2D'},
                ]}>
                Modul {props.keys}
              </Text>
            ) : null}
            <View style={{}}>
              <Text style={style.subtitle}>
                {props.spliteLengthDesc
                  ? props.subtitle
                      .split(' ')
                      .slice(0, props.spliteLengthDesc)
                      .join(' ') + ' ...'
                  : props.subtitle}
              </Text>
            </View>
          </View>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

export default withNavigation(withStyles(ListModule));

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    minHeight: 120,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    backgroundColor: '#Ffff',
    height: 120,
    marginVertical: 3,
    flexDirection: 'row',
    borderRadius: 5,
    borderBottomColor: '#eee',
    borderBottomWidth: 1,
  },
  imageContent: {
    width: '30%',
    height: '100%',
    justifyContent: 'flex-start',
  },

  image: {
    height: '100%',
    width: '100%',
    borderRadius: 5,
  },
  caption: {
    width: '70%',
    alignContent: 'center',
    justifyContent: 'flex-start',
    paddingTop: 10,
    paddingLeft: 15,
  },
  title: {
    fontSize: 14,
    flexWrap: 'wrap',
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 12,
    color: '#000',
    fontWeight: '200',
    flexWrap: 'wrap',
    paddingRight: 10,
    lineHeight: 20,
  },
  captions: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 90,
    height: 90,
    borderRadius: 5,
  },
});
