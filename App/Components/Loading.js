import React, {Component} from 'react';
import {Text} from '@ui-kitten/components';
import {Dimensions, StyleSheet, View, ActivityIndicator} from 'react-native';

const {height, width} = Dimensions.get('screen');
const Loading = props => {
  return (
    <View style={style.loadingContent}>
      <View style={style.content}>
        <Text style={style.text}>Memuat</Text>
        <ActivityIndicator size={32} color="#000" />
      </View>
    </View>
  );
};

export default Loading;

const style = StyleSheet.create({
  content: {
    backgroundColor: 'rgba(76, 175, 80, 0.3)',
    width: 130,
    height: 70,
    borderRadius: 5,
  },
  text: {
    fontSize: 18,
    textAlign: 'center',
    alignItems: 'center',
    color: '#000',
    paddingTop: 10,
  },
  loadingContent: {
    height: height / 1.3,
    width: width,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
});
