import React, {Component} from 'react';
import {Text} from '@ui-kitten/components';
import {ProgressBar, Colors} from 'react-native-paper';
import {Dimensions, StyleSheet, View} from 'react-native';

const {height, width} = Dimensions.get('screen');
const ProgressLoading = props => {
  console.log(props.progress);
  return (
    <View style={style.loadingContent}>
      <View style={style.content}>
        <Text style={style.text}>Memuat</Text>
        <Text style={[style.text, {paddingTop: -5}]}>
          {(props.progress * 100).toFixed(0)} %
        </Text>
        <ProgressBar
          progress={parseFloat(props.progress)}
          color={Colors.red800}
          style={style.progress}
        />
      </View>
    </View>
  );
};

export default ProgressLoading;

const style = StyleSheet.create({
  content: {
    backgroundColor: 'rgba(76, 175, 80, 0.3)',
    width: 150,
    height: 80,
    justifyContent: 'space-between',
    borderRadius: 5,
  },
  text: {
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 20,
    paddingHorizontal: 2,
    alignItems: 'center',
    color: '#000',
    paddingTop: 10,
  },
  loadingContent: {
    height: height / 1.3,
    width: width,
    backgroundColor: '#eee',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  progress: {
    height: 5,
  },
});
