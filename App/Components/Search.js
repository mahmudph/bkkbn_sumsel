import React, {Component, useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableNativeFeedback,
  Dimensions,
} from 'react-native';
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
const {height, width} = Dimensions.get('screen');

const Search = props => {
  let [Background, setBackground] = useState('#31467D');
  return (
    <View style={style.container}>
      <View style={style.inputstyleContent}>
        <View style={style.arrowBack}>
          <TouchableNativeFeedback
            onPress={() => props.navigation.goBack()}
            style={{backgroundColor: 'blue', paddingLeft: 10}}>
            <Icon name="md-arrow-back" size={24} color="#FFF" />
          </TouchableNativeFeedback>
        </View>
        <View style={{justifyContent: 'center'}}>
          <TextInput
            style={style.inputTextStyle}
            autoCapitalize="sentences"
            autoCompleteType="off"
            autoCorrect={false}
            autoFocus={true}
            maxLength={50}
            placeholder={props.placeholder ? props.placeholder : 'Pencarian'}
            onEndEditing={() => setBackground('#f4511e')}
            returnKeyType="search"
            placeholderTextColor={
              props.placeholder ? props.placeholder : '#fff'
            }
            onChangeText={e => props.onChangeText(e)}
          />
        </View>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    width: width,
    justifyContent: 'center',
    height: 60,
    backgroundColor: '#31467D',
  },
  arrowBack: {
    padding: 10,
    paddingLeft: 15,
    justifyContent: 'center',
    height: 60,
  },
  inputstyleContent: {
    flexDirection: 'row',
    borderRadius: 3,
    width: width,
  },
  inputTextStyle: {
    width: width - 70,
    fontSize: 18,
    borderRadius: 5,
    paddingLeft: 10,
    justifyContent: 'center',
    color: '#FFF',
  },
  iconInput: {
    width: 50,
    paddingTop: 10,
  },
});
export default withNavigation(Search);
