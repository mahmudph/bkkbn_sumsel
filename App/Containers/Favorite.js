import React, {Component, useState, useEffect} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {
  StyleSheet,
  StatusBar,
  View,
  ScrollView,
  Dimensions,
  TouchableNativeFeedback,
} from 'react-native';
import {ListModule, NotFound} from 'App/Components/';
import {selectModul, HapusItem} from 'App/Helpers/';
import {Modal} from 'react-native-paper';

const {width, height} = Dimensions.get('screen');
const Favorite = props => {
  let [item, setTemp] = useState();
  let [data, setData] = useState([]);
  let [visible, setVisible] = useState(false);

  const handleData = item => {
    setTemp(item);
    setVisible(true);
  };
  const getFavorite = async () => {
    let temp = [];
    let datas = await selectModul();
    for (var i = 0; i < datas.rows.length; ++i) {
      temp.push(datas.rows.item(i));
    }
    setData(temp);
  };

  const HapusItems = () => {
    HapusItem(item).then(result => {
      if (result) {
        let temp = data.filter(items => items.id != item.id);
        setData(temp);
        setVisible(false);
      } else {
        console.log('failed');
      }
    });
  };

  useEffect(() => {
    getFavorite();
  }, [data]);

  return (
    <Layout style={style.container}>
      <StatusBar barStyle="light-content" translucent />
      <View>
        <ScrollView>
          <View style={style.content}>
            {data.length > 0 ? (
              data.map((item, i) => (
                <ListModule
                  // image="https://i.ya-webdesign.com/images/avatar-png-1.png"
                  title={
                    item.title.charAt(0).toUpperCase() + item.title.substring(1)
                  }
                  index={i}
                  favorite
                  id={i + 1}
                  data={item}
                  subtitle={item.deskripsi ? item.deskripsi.substr(0, 90) : ''}
                  aksi={handleData}
                />
              ))
            ) : (
              <NotFound />
            )}
          </View>
        </ScrollView>
      </View>
      <Modal
        visible={visible}
        onDismiss={() => setVisible(false)}
        contentContainerStyle={style.modal}>
        <View style={style.contentButton}>
          <TouchableNativeFeedback
            onPress={() => props.navigation.navigate('Content', {data: item})}>
            <View>
              <Text style={style.modalText}>Buka Modul</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
        <View style={style.contentButton}>
          <TouchableNativeFeedback onPressIn={() => HapusItems()}>
            <Text style={style.modalText}>Hapus</Text>
          </TouchableNativeFeedback>
        </View>
      </Modal>
    </Layout>
  );
};

Favorite.navigationOptions = {
  title: 'Favorite',
  headerStyle: {
    backgroundColor: '#31467D',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    marginBottom: 20,
    backgroundColor: '#FFF',
    paddingTop: 10,
  },
  modal: {
    backgroundColor: '#FFF',
    width: width / 1.6,
    borderRadius: 3,
    alignItems: 'center',
    alignSelf: 'center',
  },
  modalText: {
    lineHeight: 29,
    fontSize: 16,
    padding: 10,
    color: '#616161',
    textAlign: 'center',
  },
  contentButton: {
    width: '100%',
  },
});

export default Favorite;
