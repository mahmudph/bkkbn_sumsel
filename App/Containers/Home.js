// @ts-nocheck
import React, {Component} from 'react';
import {Layout, Text} from '@ui-kitten/components';
// @ts-ignore
import {
  StyleSheet,
  StatusBar,
  View,
  TouchableNativeFeedback,
  Dimensions,
  ScrollView,
} from 'react-native';
import {Benner, ListModule, Loading} from 'App/Components/';
import Icon from 'react-native-vector-icons/Ionicons';
import assets from 'App/Assets';

const {height, width} = Dimensions.get('screen');

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
    };
  }
  componentDidMount() {
    import('App/module').then(data => {
      this.setState({data: data.data, loading: false});
      this.props.navigation.setParams({
        data: data.data,
      });
    });
  }

  static navigationOptions = ({navigation}) => {
    return {
      title: 'BKKBN',
      headerStyle: {
        elevation: 0,
        backgroundColor: '#31467D',
      },
      headerRight: () => (
        <TouchableNativeFeedback
          style={{borderRadius: 10, flex: 1}}
          onPress={() => {
            navigation.navigate('Search', {
              key: null,
              data: navigation.getParam('data'),
            });
          }}>
          <View style={style.searchIconContent}>
            <Icon name="ios-search" size={25} color="#FFF" />
          </View>
        </TouchableNativeFeedback>
      ),
      headerTitleAlign: 'left',
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    };
  };
  render() {
    return (
      <Layout style={style.container}>
        <StatusBar
          barStyle="light-content"
          translucent={true}
          backgroundColor={'transparent'}
        />
        {this.state.loading ? (
          <Loading />
        ) : (
          <ScrollView>
            <Text
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                color: '#616161',
                backgroundColor: '#fff',
                fontWeight: 'bold',
                marginHorizontal: 10,
                paddingTop: 10,
                fontSize: 16,
              }}>
              Pengenalan
            </Text>
            <View style={{justifyContent: 'center'}}>
              <ScrollView
                style={{marginHorizontal: 5}}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={style.scrollView}>
                <Benner color="blue" id={1} image={assets.banner.satu} />
                <Benner color="purple" id={2} image={assets.banner.dua} />
                <Benner color="green" id={3} image={assets.banner.tiga} />
              </ScrollView>
            </View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <Text
                style={{
                  color: '#616161',
                  fontWeight: 'bold',
                  fontSize: 14,
                }}>
                Modul Pembelajaran
              </Text>
              {this.state.data.map((item, i) => {
                return (
                  <ListModule
                    id={item.id}
                    data={item}
                    jml={item.data.length}
                    image={item.img}
                    key={i}
                    title={'Modul ' + item.key}
                    subtitle={item.description}
                    style={style.listModule}
                  />
                );
              })}
            </View>
            <View
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                height: 20,
                backgroundColor: '#fff',
              }}
            />
          </ScrollView>
        )}
      </Layout>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  banner: {
    paddingHorizontal: 5,
    justifyContent: 'flex-start',
  },
  scrollView: {
    backgroundColor: '#FFF',
    paddingVertical: 10,
    justifyContent: 'flex-start',
  },
  listModule: {
    height: 130,
    marginVertical: 3,
  },
  searchIconContent: {
    alignItems: 'center',
    marginRight: 20,
    padding: 5,
  },
  loadingContent: {
    height: height / 1.3,
    width: width,
    backgroundColor: '#eee',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
});

export default Home;
