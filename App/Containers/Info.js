// @ts-ignore
// eslint-disable-next-line no-unused-vars
import React, {Component, useState} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {
  StyleSheet,
  ScrollView,
  StatusBar,
  View,
  Image,
  Dimensions,
  Linking,
  ToastAndroid,
} from 'react-native';
import assets from 'App/Assets/index';
import {List, Checkbox} from 'react-native-paper';
import {withNavigation} from 'react-navigation';

const {width, height} = Dimensions.get('screen');

const Info = props => {
  let [expanded, setExpanded] = useState(false);
  const _handlePress = () => {
    setExpanded(!expanded);
  };

  const navigateToPenggunaan = () => {
    props.navigation.navigate('Penggunaan');
  };
  const tanggapiKami = async () => {
    try {
      let link =
        'https://play.google.com/store/apps/details?id=com.bkkbn_sumsel';
      let canopen = await Linking.canOpenURL(link);
      if (canopen) {
        await Linking.openURL(link);
      }
    } catch (error) {
      ToastAndroid.show('tidak bisa membuka aplikasi', 3000);
    }
  };
  return (
    <Layout style={style.container}>
      <StatusBar barStyle="light-content" translucent />
      <ScrollView>
        <View style={style.content}>
          <View
            style={{
              backgroundColor: '#3FB2BB',
              borderRadius: 100,
              justifyContent: 'center',
            }}>
            <Image
              source={assets.logo}
              style={{width: 125, height: 125, borderRadius: 200}}
              resizeMode="contain"
            />
          </View>
        </View>
        <View style={{backgroundColor: '#FFF'}}>
          <List.Section title="Informasi aplikasi">
            <List.Item
              title="Penggunaan Aplikasi"
              descriptionNumberOfLines={1}
              description="Berisi panduan singkat aplikasi"
              onPress={() => navigateToPenggunaan()}
              right={props => (
                <List.Icon {...props} icon="arrow-right-circle" />
              )}
              left={props => (
                <Image
                  source={assets.manual}
                  resizeMode="center"
                  style={style.imageIcon}
                />
              )}
            />
            <List.Item
              title="Tanggapi Kami"
              descriptionNumberOfLines={1}
              onPress={() => tanggapiKami()}
              description="Berikan Feedback untuk pengembangan lebih baik"
              left={props => (
                <Image
                  source={assets.feedback}
                  resizeMode="center"
                  style={style.imageIcon}
                />
              )}
              right={props => (
                <List.Icon {...props} icon="arrow-right-circle" />
              )}
            />

            <List.Item
              title="Informasi Aplikasi"
              descriptionNumberOfLines={1}
              onPress={() => props.navigation.navigate('Information')}
              description="Informasi Mengenai Aplikasi"
              right={props => (
                <List.Icon {...props} icon="arrow-right-circle" />
              )}
              left={props => (
                <Image
                  source={assets.question}
                  resizeMode="center"
                  style={style.imageIcon}
                />
              )}
            />
          </List.Section>
        </View>
      </ScrollView>
    </Layout>
  );
};

Info.navigationOptions = {
  title: 'Information',
  headerStyle: {
    backgroundColor: '#31467D',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    flexDirection: 'column',
  },
  content: {
    width: width,
    flexDirection: 'column',
    backgroundColor: '#31467D',
    height: height / 3.6,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    width: 90,
  },
  imageIcon: {height: 35, width: 35, justifyContent: 'center'},
  caption: {
    fontSize: 18,
    marginTop: -10,
    fontWeight: 'bold',
  },
  dev: {
    marginLeft: 20,
  },
});

export default withNavigation(Info);
