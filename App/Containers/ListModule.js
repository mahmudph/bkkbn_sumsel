/* eslint-disable react/jsx-no-comment-textnodes */
import React, {Component, useState, useEffect} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {
  StyleSheet,
  StatusBar,
  View,
  Dimensions,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconFoundation from 'react-native-vector-icons/Foundation';
import {Grid, Loading} from 'App/Components';

const {width, height} = Dimensions.get('screen');
const ListModules = props => {
  let [data, setData] = useState([]);
  useEffect(() => {
    setTimeout(() => {
      setData(props.navigation.getParam('data').data);
    }, 50);
  }, [props.navigation]);
  const iconVideo = (index, key) => {
    if (index === 14) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <IconFoundation name="play-video" size={54} color="#FFF" />
          <Text
            style={{
              flexWrap: 'wrap',
              textAlign: 'center',
              color: '#FFF',
            }}>
            Video Bina Keluarga Remaja
          </Text>
        </View>
      );
    }
  };
  return (
    <Layout style={style.container}>
      <StatusBar barStyle="light-content" translucent />
      <ScrollView>
        <View
          style={[
            style.container,
            // eslint-disable-next-line react-native/no-inline-styles
            {
              justifyContent: 'center',
              alignSelf: 'center',
            },
          ]}>
          <View
            style={{width: '100%', justifyContent: 'center', padding: 'auto'}}>
            <View
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                marginVertical: 3,
                marginHorizontal: 2,
                justifyContent: 'space-between',
              }}>
              {data.length > 0 ? (
                data.map((item, index) => (
                  <Grid
                    key={index}
                    index={index}
                    data={item}
                    image={item.imagename}
                    id_module={props.navigation.getParam('title')}>
                    {iconVideo(index, props.navigation.getParam('title'))}
                  </Grid>
                ))
              ) : (
                <Loading />
              )}
            </View>
          </View>
        </View>
      </ScrollView>
    </Layout>
  );
};

ListModules.navigationOptions = ({navigation}) => {
  return {
    title: navigation.getParam('title'),
    headerStyle: {
      backgroundColor: '#31467D',
    },
    headerRight: () => (
      <TouchableWithoutFeedback
        onPress={() =>
          navigation.navigate('Search', {
            key: navigation.getParam('key'),
            data: navigation.getParam('data').data,
          })
        }>
        <View style={style.searchIconContent}>
          <Icon name="ios-search" size={25} color="#FFF" />
        </View>
      </TouchableWithoutFeedback>
    ),
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  searchIconContent: {
    marginRight: 20,
  },
  content: {
    width: width / 2,
    borderRadius: 5,
    height: height / 1.6,
  },
});

export default ListModules;
