/* eslint-disable react-native/no-inline-styles */
// @ts-ignore
import React, {Component, useState, useEffect} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {
  StyleSheet,
  StatusBar,
  View,
  Dimensions,
  ScrollView,
  TouchableNativeFeedback,
  ToastAndroid,
} from 'react-native';
import {Loading, Card} from 'App/Components';
import {insertFavorite} from 'App/Helpers';
import assets from 'App/Assets/';
import {fileExistinFolder, hasFavorite} from 'App/Helpers';
import Icon from 'react-native-vector-icons/AntDesign';
import {withNavigation} from 'react-navigation';
// @ts-ignore
const {height, width} = Dimensions.get('screen');

const saveToFavorite = async navigation => {
  try {
    let temp = await navigation.getParam('data');
    let id_module = await navigation.getParam('id_module');

    let tempToSave = {
      id_module: id_module,
      id_data: temp.data_id,
      title: temp.title,
      imagename: temp.imagename,
      youtubeUrl: temp.youtubeUrl,
      deskripsi: temp.deskripsi,
      ppt: temp.ppt,
    };

    let result = await insertFavorite(tempToSave);
    if (result) {
      ToastAndroid.show('Berhasil Add ke favorite', 3000);
    } else {
      ToastAndroid.show('Data sudah di tambahkan', 3000);
    }
  } catch (err) {
    console.log(err);
  }
};

const Module_content = props => {
  let [data, setData] = useState({});
  let [loading, setLoading] = useState(true);
  useEffect(() => {
    const getdata = async () => {
      try {
        let temp = await props.navigation.getParam('data');
        let hasFavoriteItem = await hasFavorite(temp);
        await props.navigation.setParams({favorite: hasFavoriteItem});
        setData(temp);
        setLoading(false);
      } catch (err) {
        console.log(err);
      }
    };
    getdata();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const openVideo = () => {
    // @ts-ignore
    if ('videoUrl' in data && data.videoUrl != null) {
      props.navigation.navigate('PlayVideo', {
        data: data,
        key: props.navigation.getParam('key'),
      });
    } else {
      ToastAndroid.show('Materi ini tidak memiliki video', 3000);
    }
  };

  const openPdf = () => {
    if ('ppt' in data && data.ppt != null) {
      props.navigation.navigate('PdfViewer', {data: data});
    } else {
      ToastAndroid.show('Materi ini tidak memiliki pdf', 3000);
    }
  };
  return (
    <Layout style={style.container}>
      <StatusBar barStyle="light-content" translucent />
      <View style={style.container}>
        {!loading ? (
          <View>
            <Card
              image={assets.books}
              title={data.title}
              id_module={props.navigation.getParam('key')}
              pertemuan={data.id}
              openVideo={openVideo}
              openPdf={openPdf}
              data={data}
            />
            <ScrollView>
              <View
                style={{
                  paddingHorizontal: 20,
                  paddingTop: 20,
                  backgroundColor: '#FFF',
                }}>
                <Text style={style.about}>Tentang Materi</Text>
                <Text style={style.subtitle}>{data.deskripsi}</Text>
              </View>
            </ScrollView>
          </View>
        ) : (
          <Loading />
        )}
      </View>
    </Layout>
  );
};

Module_content.navigationOptions = ({navigation}) => {
  return {
    title: 'Materi',
    headerStyle: {
      backgroundColor: '#31467D',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerRight: () => {
      let hasFavoritese = navigation.getParam('favorite');
      return (
        <TouchableNativeFeedback onPress={() => saveToFavorite(navigation)}>
          <View style={style.searchIconContent}>
            <Icon
              name="hearto"
              size={22}
              color={hasFavoritese ? 'red' : 'yellow'}
            />
          </View>
        </TouchableNativeFeedback>
      );
    },
  };
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFF',
  },
  subtitle: {
    fontSize: 14,
    marginTop: 5,
    color: '#000',
    width: '100%',
    paddingTop: 10,
    textAlign: 'justify',
    fontFamily: 'Roboto',
    flexWrap: 'wrap',
  },
  about: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  hapusMateri: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: '#FF3800',
  },
  itemHapus: {
    padding: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignContent: 'center',
  },
  itemHapusText: {
    fontSize: 18,
    color: '#FFF',
    fontWeight: 'bold',
  },
  searchIconContent: {
    alignItems: 'center',
    padding: 10,
    marginRight: 10,
  },
});

export default withNavigation(Module_content);
