import React, {useEffect, useState} from 'react';
import {View, StatusBar, StyleSheet, ScrollView} from 'react-native';
import {Layout} from '@ui-kitten/components';
import {ListModule, Loading} from 'App/Components/';

const ListPertemuan = props => {
  let [data, setData] = useState([]);
  useEffect(() => {
    const GetData = async () => {
      let temp = await props.navigation.getParam('data');
      if (temp != null) {
        setData(temp);
      }
    };
    GetData();
  }, [props.navigation]);
  console.log(props.navigation.getParam('key'));
  const handleData = item => {
    /*  if (item.videoUrl || ('videoUrl' in item && item.ppt == null)) {
      props.navigation.navigate('PlayVideo', {
        data: item,
        key: props.navigation.getParam('key'),
      });
    } else { */
    props.navigation.navigate('Content', {
      data: item,
      key: props.navigation.getParam('key'),
    });
    // }
  };
  return (
    <Layout style={style.container}>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="light-content"
      />
      <ScrollView>
        <View
          style={{
            marginHorizontal: 5,
          }}>
          {data.length > 0 ? (
            data.map((item, index) => (
              <ListModule
                id={index + 1}
                data_id={item.data_id}
                data={item}
                splite={true}
                spliteLengthTitle={8}
                spliteLengthDesc={10}
                favorite
                subtitle={item.deskripsi}
                image={item.imagename}
                title={'judul' in item ? item.judul : item.title}
                aksi={handleData}
                style={{}}
              />
            ))
          ) : (
            <Loading />
          )}
        </View>
      </ScrollView>
    </Layout>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
});

ListPertemuan.navigationOptions = {
  title: 'Materi',
  headerStyle: {
    backgroundColor: '#31467D',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

export default ListPertemuan;
