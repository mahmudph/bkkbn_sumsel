// @ts-nocheck
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState, useReducer} from 'react';
// @ts-ignore
import {Layout} from '@ui-kitten/components';
import {
  StyleSheet,
  StatusBar,
  View,
  BackHandler,
  Alert,
  TouchableNativeFeedback,
  ToastAndroid,
  PermissionsAndroid,
  Dimensions,
} from 'react-native';
import Video from 'react-native-video';
import Orientation from 'react-native-orientation';
import Icon from 'react-native-vector-icons/Ionicons';
import {downloadFileVideo, fileExistinFolder} from 'App/Helpers';
import {deleteFilebyPath} from 'App/Helpers';
import {ActivityIndicator} from 'react-native-paper';
import {AlertComponent} from 'App/Components';

const initialState = {
  data: [],
};
const reducer = (state, action) => {
  switch (action.type) {
    case 'ADD_DOWNLOAD':
      let temp = state.data;
      temp.push(action.payload);
      return {...state, data: temp};
    case 'CHECK_DOWNLOAD':
      return state.data.map((item, i) => item.title === action.payload.title);
    case 'CHANGE_DOWNLOAD_STATUS':
      let items = state.data.filter((item, i) => {
        if (item.title === action.payload.title) {
          item.statusDownload = 'done';
          return item;
        }
      });
      return {...state, data: items};

    case 'DELETE_DOWNLOAD':
      let tmp = state.data.filter((item, i) => {
        if (item.title !== action.payload.title) {
          return item;
        }
      });
      return {...state, data: tmp};
    case 'STREAMING_VIDEO':
      let temps = state.data;
      temps.push(action.payload);
      return {...state, data: temps};

    default:
      return state;
  }
};

const PlayVideo = props => {
  let [data, setData] = useState(true);
  let [videoPath, setVideoPath] = useState('');
  let [dialog, setDialog] = useState(false);
  let [message, setMessage] = useState('');
  let [reload, setReload] = useState(false);
  let [buffer, setBuffer] = useState(true);
  let [backupUrlVideo, setBackupUrlVideo] = useState('');
  let [countError, setCountError] = useState(0);
  let [screen, setScreen] = useState('portaid');
  let [pause, setPause] = useState(false);
  let [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    Orientation.unlockAllOrientations();
    const backHandlerSet = () => {
      if (props.navigation.state.routeName === 'PlayVideo') {
        Alert.alert(
          'Alert',
          'Apa anda ingin keluar?',
          [
            {text: 'Keluar', onPress: () => props.navigation.goBack()},
            {text: 'Tidak', onPress: () => {}},
          ],
          {cancelable: false},
        );
        return true;
      } else {
        return true;
      }
    };
    const handleBack = BackHandler.addEventListener(
      'hardwareBackPress',
      backHandlerSet,
    );

    const getsomeData = async () => {
      try {
        let permision = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        let tenp = await props.navigation.getParam('data');
        let folderName = await props.navigation.getParam('key');
        let pathRootSave = folderName.replace(' ', '_').toLowerCase();
        let realPathSave = `${pathRootSave}/${tenp.videoUrl
          .split('?')
          .shift()
          .split('/')
          .slice(-2)
          .join('/')}`;
        let fileExsist = await fileExistinFolder(decodeURI(realPathSave));
        let urlvideopath =
          'streamUrl' in tenp || tenp.streamUrl != null
            ? tenp.streamUrl
            : tenp.videoUrl;
        setBackupUrlVideo(urlvideopath);
        setData(tenp);
        if (permision) {
          if (fileExsist) {
            setVideoPath(fileExsist);
            ToastAndroid.show('Anda Memutar Secara Ofline', 3000);
            dispatch({
              type: 'ADD_DOWNLOAD',
              payload: {
                title: tenp.title,
                statusDownload: 'saveLocal',
              },
            });
          } else {
            let hasdownloadvideo = state.data.map((item, i) => {
              if (
                item.title === tenp.title &&
                item.statusDownload !== 'progress'
              ) {
                return true;
              }
              return false;
            });
            //jika proses download belum pernah dilakukan maka download video
            setVideoPath(urlvideopath);
            if (hasdownloadvideo) {
              Alert.alert(
                'Download video?',
                'Apa anda ingin menyipan materi ini?',
                [
                  {
                    text: 'Download',
                    onPress: () => {
                      let setPayload = {
                        title: tenp.title,
                        statusdownload: 'progress',
                      };
                      // @ts-ignore
                      dispatch({type: 'ADD_DOWNLOAD', payload: setPayload});
                      downloadFileVideo(tenp.videoUrl, realPathSave)
                        .then(res => {
                          ToastAndroid.show('Video berhasil didownload', 3000);
                          dispatch({
                            type: 'CHANGE_DOWNLOAD_STATUS',
                            payload: {title: data.title},
                          });
                        })
                        .catch(() => {
                          Alert.alert(
                            'Alert',
                            'Gagal mendownload video. Pastikan anda memiliki koneksi internet',
                            [
                              {
                                text: 'oke',
                                onPress: () => {
                                  dispatch({
                                    type: 'DELETE_DOWNLOAD',
                                    payload: {title: data.title},
                                  });
                                  props.navigation.goBack();
                                },
                                style: 'cancel',
                              },
                            ],
                            {cancelable: false},
                          );
                        });
                    },
                  },
                  {
                    text: 'Tidak',
                    onPress: () => {
                      dispatch({
                        type: 'STREAMING_VIDEO',
                        payload: {
                          title: tenp.title,
                          progress: 'streaming',
                        },
                      });
                    },
                  },
                ],
                {cancelable: false},
              );
            } else {
            }
          }
        } else {
          alert('Aplikasi membutuhkan akses untuk memutar file ini');
          getsomeData();
        }
      } catch (err) {
        console.error(err);
      }
    };
    getsomeData();
    const deviceOrientation = Dimensions.addEventListener('change', e => {
      if (e.screen.width > e.screen.height) {
        setScreen('Landscape');
      } else {
        setScreen('portaid');
      }
    });
    return () => {
      setDialog(false);
      Orientation.lockToPortrait();
      if (handleBack) {
        handleBack.remove();
      } else if (deviceOrientation) {
        deviceOrientation.remove();
      }
    };
  }, [reload]);

  const simpanitem = () => {
    setDialog(false);
    props.navigation.goBack();
  };
  const hapusFile = () => {
    Alert.alert(
      'Alert',
      'Apa anda yakin untuk menghapus video ini?',
      [
        {
          text: 'ya',
          onPress: () => {
            deleteFilebyPath(videoPath)
              .then(res => {
                setDialog(false);
                ToastAndroid.show('File berhasi dihapus', 2000);
                props.navigation.goBack();
              })
              .catch(() => {});
          },
        },
        {text: 'Tidak', onPress: () => {}},
      ],
      {cancelable: false},
    );
  };
  const handleLoadStart = () => {
    setBuffer(false);
  };
  const renderVideo = () => {
    if (videoPath) {
      return (
        <Video
          paused={pause}
          onLoad={handleLoadStart}
          playInBackground={true}
          controls={true}
          resizeMode="contain"
          onBuffer={handleBuffer}
          ref={vi => {}}
          source={{
            uri: videoPath,
          }}
          onError={err => {
            console.error(err);
            console.log(countError);
            console.log(err);
            if (err.error.extra == -2147483648) {
              deleteFilebyPath(videoPath)
                .then(() => {})
                .catch(() => {})
                .finally(() => {
                  setVideoPath(backupUrlVideo);
                });
            } else {
              if (countError >= 5) {
                alert('Pastikan anda terhubung dengan internet');
                setReload(true);
                dispatch({
                  type: 'DELETE_DOWNLOAD',
                  payload: {title: data.title},
                });
              } else {
                setCountError(val => val + 1);
              }
            }
          }}
          style={style.backgroundVideo}
        />
      );
    } else {
      return (
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
          }}>
          <View
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              borderRadius: 5,
              width: 70,
              height: 70,
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <ActivityIndicator size="small" />
          </View>
        </View>
      );
    }
  };

  const handleBuffer = meta => {
    console.log(meta);
    setBuffer(meta.isBuffering);
  };
  const spinerLoading = () => {
    if (buffer) {
      return (
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
          }}>
          <View
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              borderRadius: 5,
              width: 70,
              height: 70,
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <ActivityIndicator size="small" />
          </View>
        </View>
      );
    } else {
      return null;
    }
  };

  return (
    <Layout>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        hidden={screen === 'portaid' ? false : true}
        backgroundColor={'transparent'}
      />
      <View style={style.content}>
        {videoPath.includes('storage') ? (
          <View
            style={[
              style.exitIcon,
              // eslint-disable-next-line react-native/no-inline-styles
              {
                top: screen === 'portaid' ? StatusBar.currentHeight : 0,
                margin: 10,
                borderRadius: 10,
                backgroundColor: 'rgba(255,255,255,0.5)',
              },
            ]}>
            <TouchableNativeFeedback onPress={() => hapusFile()}>
              <Icon
                name="ios-trash"
                size={28}
                color="#eee"
                style={{justifyContent: 'center'}}
              />
            </TouchableNativeFeedback>
          </View>
        ) : null}
        {renderVideo()}
        <AlertComponent
          dialog={dialog}
          onDismiss={() => setDialog(false)}
          message={message}
          simpan={simpanitem}
          callbackDismis={() => {
            setDialog(false);
            setMessage(null);
            props.navigation.goBack();
          }}
          hapusFile={hapusFile}
        />
        {spinerLoading()}
      </View>
    </Layout>
  );
};

// @ts-ignore
PlayVideo.navigationOptions = () => {
  return {
    headerShown: false,
    headerMode: 'screen',
  };
};

const style = StyleSheet.create({
  content: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  contentVideo: {
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'center',
  },
  backgroundVideo: {
    width: '100%',
    height: '100%',
  },
  exitIcon: {
    position: 'absolute',
    right: 0,
    top: StatusBar.currentHeight,
    zIndex: 100,
    justifyContent: 'flex-end',
    margin: 10,
    padding: 15,
  },
});

export default PlayVideo;
