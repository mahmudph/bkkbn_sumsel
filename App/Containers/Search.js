import React, {Component, lazy, Suspense} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {StyleSheet, StatusBar, View, ScrollView, Platform} from 'react-native';
import {ListModule, Search, NotFound, Loading} from 'App/Components/';

const Searchs = lazy(() => import('App/Components/Search'));

class search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backup: [],
      data: [],
    };
    this.data = this.props.navigation.getParam('data');
  }

  componentDidMount() {
    this.getParamAndData().then(res => {
      this.setState({data: res.slice(0, 10), backup: res});
    });
  }

  shouldComponentUpdate(prevProps, prevState) {
    if (prevState.data !== this.state.data) {
      return true;
    }
  }

  filter = async word => {
    let temp = [];
    let {backup} = this.state;
    for (var i = 0; i < backup.length; i++) {
      let kondisi =
        (await backup[i].title.toLowerCase().indexOf(word.toLowerCase())) >= 0;
      if (kondisi) {
        temp.push(backup[i]);
      }
    }

    return temp;
  };

  getParamAndData = async () => {
    try {
      let tamp = [];
      let id = await this.props.navigation.getParam('key');
      // let data = await this.props.navigation.getParam('data');
      if (id) {
        for (var item of this.data) {
          if ('item' in item) {
            item.item.forEach((itm, x) => {
              tamp.push(itm);
            });
          } else {
            tamp.push(item);
            console.log(item);
          }
        }
        return tamp;
      } else {
        for (let i = 0; i < this.data.length; i++) {
          for (let j = 0; j < this.data[i].data.length; j++) {
            if ('item' in this.data[i].data[j]) {
              this.data[i].data[j].item.forEach((tem, x) => {
                tamp.push(tem);
              });
            } else {
              tamp.push(this.data[i].data[j]);
            }
          }
        }
        return tamp;
      }
    } catch (err) {
      console.log('errror disini', err);
    }
  };

  handleData = data => {
    console.log(data);
    this.props.navigation.navigate('Content', {
      data: data,
      key: `MODUL ${data.key}`,
    });
  };

  getBackInput = e => {
    this.filter(e).then(result => {
      this.setState({data: result.slice(0, 10)});
    });
  };
  render() {
    return (
      <Layout style={style.content}>
        <StatusBar
          barStyle="light-content"
          translucent
          backgroundColor="#31467D"
        />
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            marginTop: Platform.OS == 'android' ? StatusBar.currentHeight : 20,
          }}>
          <Suspense fallback={<Loading />}>
            <Searchs onChangeText={this.getBackInput} />
          </Suspense>
          <View style={style.listItem}>
            <ScrollView>
              {this.state.data.length > 0 ? (
                this.state.data.map((item, i) => (
                  <ListModule
                    id={this.props.navigation.getParam('key')}
                    key={i}
                    data_id={item.data_id}
                    data={item}
                    favorite
                    index={i}
                    keys={item.key}
                    title={item.title}
                    spliteLengthTitle={9}
                    spliteLengthDesc={8}
                    subtitle={item.deskripsi}
                    aksi={this.handleData}
                  />
                ))
              ) : (
                <NotFound />
              )}
            </ScrollView>
          </View>
        </View>
      </Layout>
    );
  }
}

search.navigationOptions = {
  headerShown: false,
  headerStyle: {
    backgroundColor: '#f4511e',
  },
};

const style = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  imageSearch: {
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
  listItem: {
    marginTop: 3,
    flexDirection: 'column',
  },
});

export default search;
