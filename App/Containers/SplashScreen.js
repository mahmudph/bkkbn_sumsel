import React, {useEffect} from 'react';
import {View} from 'react-native';
import Splashscreen from 'react-native-splash-screen';

const splashScreen = props => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useEffect(() => {
    Splashscreen.hide();
    props.navigation.navigate('AppScreen');
  }, [props.navigation]);

  return <View />;
};

splashScreen.navigationOptions = {
  header: null,
};

export default splashScreen;
