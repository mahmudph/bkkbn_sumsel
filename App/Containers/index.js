import Home from './Home';
import Info from './Info';
import Favorite from './Favorite';
import SplashScreen from './SplashScreen';
import search from './Search';
import ListModules from './ListModule';
import Content from './Modulcontent';
import ListPertemuan from './PertemuanList';
import PdfViewer from './pdfView';
import PlayVideo from './PlayVideo';
import Penggunaan from './penggunaan';
import Information from './information';
export {
  SplashScreen,
  Information,
  ListModules,
  Content,
  Home,
  Favorite,
  Info,
  search,
  ListPertemuan,
  PdfViewer,
  PlayVideo,
  Penggunaan,
};
