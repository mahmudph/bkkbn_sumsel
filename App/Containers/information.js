// @ts-ignore
// eslint-disable-next-line no-unused-vars
import React, {Component, useState} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {
  StyleSheet,
  ScrollView,
  StatusBar,
  View,
  Image,
  Dimensions,
  Linking,
  ToastAndroid,
} from 'react-native';
import assets from 'App/Assets/index';
import {List, Checkbox} from 'react-native-paper';
import {withNavigation} from 'react-navigation';

const {width, height} = Dimensions.get('screen');

const Information = props => {
  return (
    <Layout style={style.container}>
      <StatusBar barStyle="light-content" translucent />
      <View
        // eslint-disable-next-line react-native/no-inline-styles
        style={{
          flex: 1,
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <Image source={assets.coverApp} style={style.image} />
        <Text style={style.caption}>@Copyright by Bkkbn</Text>
        <Text style={[style.caption, {fontSize: 14}]}>
          Sumatera Selatan 2019
        </Text>
        <Text style={[style.caption, {fontSize: 14}]}>PropsCode Dev</Text>
        <Text style={[style.caption, {fontSize: 14}]}>2019</Text>
      </View>
    </Layout>
  );
};

Information.navigationOptions = {
  title: 'Informasi Aplikasi',
  headerStyle: {
    backgroundColor: '#31467D',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFF',
  },
  image: {
    justifyContent: 'center',
    alignSelf: 'center',
    resizeMode: 'center',
    width: 200,
    height: 90,
  },
  caption: {
    position: 'relative',
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  dev: {
    marginLeft: 20,
  },
});

export default withNavigation(Information);
