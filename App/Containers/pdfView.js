/* eslint-disable prettier/prettier */
import React, {useEffect, useState} from 'react';
import { View, StatusBar, StyleSheet,Alert, BackHandler, ToastAndroid } from 'react-native';
import {Layout} from '@ui-kitten/components';
import Pdf from 'react-native-pdf';
import { ProgressLoading,Loading, AlertComponent } from 'App/Components';
import { deleteFilebyPath} from 'App/Helpers';
import Orientation from 'react-native-orientation';


const PdfViewer = props => {
  let [dialog, setDialog] = useState(false);
  let [progress, setProgress] = useState(0);
  let [message, setMessage] = useState('');
  let [pathFile, setPathFile] = useState('');
  let [uri, setUri] = useState({uri: null, cache:true});

  const hapusFile = () => {
    setDialog(false);
    deleteFilebyPath(pathFile).then(res => {
      if (res) {
        ToastAndroid.show('file berhasi dihapus', 2000);
        props.navigation.goBack();
      } else {
        ToastAndroid.show('file gagal dihapus', 2000);
      }
    }).catch(err => {
      console.log(err);
    });
  };

  useEffect(() => {
    Orientation.unlockAllOrientations();
    const getdata = async () => {
      try {
        let res = await props.navigation.getParam('data');
        setUri({ uri: res.ppt, cache: true });
      } catch (err) {
      }
    };
    getdata();
    const backHandlerSet = () => {
      if (props.navigation.state.routeName === 'PdfViewer') {
        console.log(progress);
        if (progress != null) {
          Alert.alert(
            'Alert',
            'Apa anda ingin keluar',
            [
              {text: 'Keluar', onPress: () => props.navigation.goBack()},
              {text: 'Tidak', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
        } else {
          setDialog(true);
        }
        return true;
      } else {
        return true;
      }
    };
    const _didFocusSubscription = BackHandler.addEventListener('hardwareBackPress', backHandlerSet);
    return () => {
      if (_didFocusSubscription) {
        _didFocusSubscription.remove();
        Orientation.lockToPortrait();
      }
    };
  },[pathFile, progress, props.navigation]);

  const progressbar = (percent) => {
    setProgress(percent.toFixed(2));
  };

  const simpanitem = () => {
      setDialog(false);
      props.navigation.goBack();
  };
  return (
    <Layout style={style.container}>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="light-content"
      />
      <View style={style.content}>
        { uri.uri ?
          <Pdf
            maxScale={2}
            onLoadProgress={progressbar}
            activityIndicator={<ProgressLoading progress={progress}/>}
            source={uri}
              style={style.pdf}
              onLoadComplete={(numberOfPages, filepath )=> {
                setPathFile(filepath);
                setMessage(null);
                setProgress(null);
              }}
              onPageChanged={page => {
                console.log(`current page: ${page}`);
              }}
            onError={error => {
              console.log(error);
                setMessage('Pastikan Anda Memiliki Jaringan Internet');
                setDialog(true);
              }}
          /> : <Loading/>
        }

        <AlertComponent
          dialog={dialog}
          onDismiss={()=> setDialog(false)}
          message={message}
          simpan={simpanitem}
          callbackDismis={()=> setDialog(false)}
          hapusFile={hapusFile}
        />
      </View>
    </Layout>
  );
};


PdfViewer.navigationOptions = ({navigation}) => {
  return {
    headerShown:false,
  };
};


const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  content: {
    flex:1,
    width: '100%',
    height:'100%',
    justifyContent: 'flex-start',
    resizeMode:'contain',
  },
  pdf: {
    flex: 1,
    flexDirection:'column',
  },
  searchIconContent: {
    alignItems: 'center',
    marginRight: 20,
  },
});

export default PdfViewer;
