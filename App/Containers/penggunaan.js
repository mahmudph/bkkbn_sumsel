// @ts-ignore
// eslint-disable-next-line no-unused-vars
import React, {Component, useState} from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {
  StyleSheet,
  ScrollView,
  StatusBar,
  View,
  Image,
  Dimensions,
} from 'react-native';
import assets from 'App/Assets/index';
import {List, Checkbox, Paragraph} from 'react-native-paper';
const {width, height} = Dimensions.get('screen');

const Penggunaan = ({props}) => {
  let [expanded, setExpanded] = useState(false);
  const _handlePress = () => {
    setExpanded(!expanded);
  };
  return (
    <Layout style={style.container}>
      <StatusBar barStyle="light-content" translucent />
      <ScrollView>
        <View>
          <List.Section title="Informasi Penggunaan Aplikasi">
            <List.Accordion
              title="Membuka materi?"
              descriptionNumberOfLines={2}
              description="Informasi mengenai membuka materi pada pertama kalinya"
              left={props => <List.Icon {...props} icon="information" />}>
              <Paragraph style={style.paragraf}>
                Anda bisa membuka materi dengan melalui form pencarian atau
                mencarinya secara langsung pada modul tertentu.
              </Paragraph>
            </List.Accordion>
            <List.Accordion
              title="Materi tidak bisa dibuka?"
              descriptionNumberOfLines={2}
              left={props => <List.Icon {...props} icon="information" />}
              description="Informasi mengenai aplikasi tidak bisa dibuka">
              <Paragraph style={style.paragraf}>
                Untuk dapat membuka materi yang terdapat pada modul, anda
                diharuskan untuk memiliki koneksi internet. Penggunaan internet
                digunakan untuk mendownload materi pada pertama kalinya
              </Paragraph>
            </List.Accordion>
            <List.Accordion
              title="Aplikasi dapat digunakan ofline?"
              descriptionNumberOfLines={2}
              description="Informasi mengenai aplikasi dapat bekerja secara ofline"
              left={props => <List.Icon {...props} icon="information" />}>
              <Paragraph style={style.paragraf}>
                Aplikasi dapat digunakan secara ofline jika materi-materi sudah
                di download pada waktu sebelumnya.
              </Paragraph>
            </List.Accordion>
          </List.Section>
        </View>
      </ScrollView>
    </Layout>
  );
};

Penggunaan.navigationOptions = {
  title: 'Penggunaan Aplikasi',
  headerStyle: {
    backgroundColor: '#31467D',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFF',
  },
  paragraf: {
    flexWrap: 'wrap',
    color: '#7622EE',
    marginLeft: 8,
    width: width * 0.96,
  },
});

export default Penggunaan;
