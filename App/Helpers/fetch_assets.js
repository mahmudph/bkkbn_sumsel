import RNFetchBlob from 'rn-fetch-blob';

const dirs = RNFetchBlob.fs.dirs;

const MKdir = async name => {
  try {
    await RNFetchBlob.fs.mkdir(dirs.SDCardDir + '/' + name);
  } catch (err) {}
};

/**
 * @param {string} link
 * @param {string} pathsave
 */
const downloadFileVideo = (link, pathsave) => {
  return new Promise(async (resolve, reject) => {
    try {
      await MKdir('modul_tribina');
      let config = {
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          description: 'Mendownload file materi',
          path: dirs.SDCardDir + '/modul_tribina/' + pathsave,
        },
      };

      // console.log(config);
      let data = RNFetchBlob.config(config);
      let res = await data.fetch('GET', link, {});
      console.info(res.path(), 'didalam sini uy');
      resolve(res);
    } catch (err) {
      console.log('errrrr', err);
      reject(err);
    }
  });
};

const deleteFile = data => {
  return new Promise(async (resolve, reject) => {
    try {
      let pathFileVideo =
        dirs.SDCardDir + '/video' + data.videoUrl.split('/').pop();
      let pathFilePdf = dirs.SDCardDir + data.ppt.split('/').pop();

      let exsistpdf = await RNFetchBlob.fs.exists(pathFilePdf);
      let exsistVideo = await RNFetchBlob.fs.exists(pathFileVideo);
      if (exsistpdf) {
        await RNFetchBlob.fs.unlink(pathFilePdf);
      }
      if (exsistVideo) {
        await RNFetchBlob.fs.unlink(pathFileVideo);
      }

      resolve('berhaasil menghapus materi');
    } catch (err) {
      reject('gagal menghapus materi');
    }
  });
};

const fileExistinFolder = async name => {
  return new Promise(async (resolve, reject) => {
    try {
      let pathFile = dirs.SDCardDir + '/modul_tribina/' + name;
      let exst = await RNFetchBlob.fs.exists(pathFile);
      resolve(exst ? pathFile : null);
    } catch (err) {
      reject(err);
    }
  });
};

const deleteFilebyPath = path => {
  return new Promise(async (resolve, reject) => {
    try {
      let exsist = await RNFetchBlob.fs.exists(path);
      console.log(exsist);
      if (exsist) {
        await RNFetchBlob.fs.unlink(path);
        resolve(true);
      } else {
        resolve(false);
      }
    } catch (err) {}
  });
};

export {downloadFileVideo, deleteFilebyPath, deleteFile, fileExistinFolder};
