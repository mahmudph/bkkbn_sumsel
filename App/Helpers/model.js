import SQLite from 'react-native-sqlite-2';

const db = SQLite.openDatabase('database.db', '1.0', '', 1);
const selectModul = () => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      const query =
        'CREATE TABLE IF NOT EXISTS favorite(id INTEGER PRIMARY KEY NOT NULL, id_module INTEGER, id_data INTEGER, title VARCHAR(50), imagename VARCHAR(50), youtubeUrl VARCHAR(50), deskripsi VARCHAR(100), ppt VARCHAR(50))';
      txn.executeSql(query, []);
      txn.executeSql('SELECT * FROM favorite', [], (tx, res) => {
        resolve(res);
      });
    });
  });
};

const insertFavorite = data => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      let checkId = 'SELECT * FROM favorite where `title`=?';
      let datas = [
        data.id_module,
        data.id_data,
        data.title,
        data.imagename,
        data.youtubeUrl,
        data.deskripsi,
        data.ppt,
      ];
      const query =
        'CREATE TABLE IF NOT EXISTS favorite(id INTEGER PRIMARY KEY NOT NULL, id_module INTEGER, id_data INTEGER, title VARCHAR(50), imagename VARCHAR(50), youtubeUrl VARCHAR(50), deskripsi VARCHAR(100), ppt VARCHAR(50))';
      txn.executeSql(query, []);
      txn.executeSql(checkId, [data.title], (tx, result) => {
        if (result.rows.length === 0) {
          txn.executeSql(
            'INSERT INTO favorite (id_module, id_data, title, imagename, youtubeUrl, deskripsi, ppt) VALUES (:id_module, :id_data, :title, :imagename, :youtubeUrl,:deskripsi, :ppt)',
            datas,
          );
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  });
};

const hasFavorite = data => {
  return new Promise((resolve, reject) => {
    db.transaction(txn => {
      let checkId = 'SELECT * FROM favorite where `title`=?';

      const query =
        'CREATE TABLE IF NOT EXISTS favorite(id INTEGER PRIMARY KEY NOT NULL, id_module INTEGER, id_data INTEGER, title VARCHAR(50), imagename VARCHAR(50), youtubeUrl VARCHAR(50), deskripsi VARCHAR(100), ppt VARCHAR(50))';
      txn.executeSql(query, []);
      txn.executeSql(checkId, [data.title], (tx, result) => {
        if (result.rows.length > 0) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  });
};

const HapusItem = data => {
  return new Promise(resolve => {
    db.transaction(txn => {
      let query = 'DELETE FROM favorite WHERE title=?';
      let datas = [data.title];
      txn.executeSql(query, datas, (ex, res) => {
        if (res.rowsAffected > 0) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
      resolve(true);
    });
  });
};

export {selectModul, insertFavorite, HapusItem, hasFavorite};
