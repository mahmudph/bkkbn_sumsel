import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  Home,
  Favorite,
  Info,
  SplashScreen,
  search,
  ListModules,
  Content,
  ListPertemuan,
  Penggunaan,
  Information,
  PdfViewer,
  PlayVideo
} from 'App/Containers';
import React, { Component } from 'react';
import { View } from 'react-native';

/* create home navigation */
const HomeContainer = createStackNavigator({
  Home: { screen: Home },
  Search: { screen: search },
  ListModules: { screen: ListModules },
  Content: { screen: Content },
  ListPertemuan: { screen: ListPertemuan },
  PdfViewer: { screen: PdfViewer },
  PlayVideo: {screen: PlayVideo},
});


HomeContainer.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map(route => {
      if (route.routeName === "PdfViewer" || route.routeName === "Content" || route.routeName === 'PlayVideo') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }
  return {tabBarVisible}
};

  /* create Favorite navigationn */
  const FavoriteConatiner = createStackNavigator({
    Favorite: { screen: Favorite },
  });

  /* create home navigationn */
  const InfoConatiner = createStackNavigator({
    Info: { screen: Info },
    Penggunaan: { screen: Penggunaan },
    Information: {screen: Information}
  });

  /* create bottom navigation  */
  const BottomTab = createMaterialBottomTabNavigator(
    {
      Home: {
        screen: HomeContainer,
        navigationOptions: {
          tabBarLabel: 'Home',
          tabBarIcon: ({ tintColor }) => (
            <View>
              <Icon style={[{ color: tintColor }]} size={25} name="ios-home" />
            </View>
          ),
        },
      },
      Favorite: {
        screen: FavoriteConatiner,
        navigationOptions: {
          tabBarLabel: 'Favorite',
          tabBarIcon: ({ tintColor }) => (
            <View>
              <Icon style={[{ color: tintColor }]} size={25} name="ios-bookmarks" />
            </View>
          ),
        },
      },
      Info: {
        screen: InfoConatiner,
        navigationOptions: {
          tabBarLabel: 'Info',
          tabBarIcon: ({ tintColor }) => (
            <View>
              <Icon
                style={[{ color: tintColor }]}
                size={25}
                name="ios-information-circle-outline"
              />
            </View>
          ),
        },
      },
    },
    {
      initialRouteName: 'Home',
      backBehavior: 'history',
      activeColor: '#1E1E1E',
      inactiveColor: '#757575',
      barStyle: { backgroundColor: '#FFFFFF' },
    },
  );

  // splashScreen: {screen: SplashScreen},
const createSwitchApp = createSwitchNavigator({
    splashScreen: {screen: SplashScreen},
    AppScreen: { screen: BottomTab },
  });

  export default createAppContainer(createSwitchApp);
