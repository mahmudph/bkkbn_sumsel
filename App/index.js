import React from 'react';
import {ApplicationProvider} from '@ui-kitten/components';
import {mapping, light as lightTheme} from '@eva-design/eva';
import {Provider} from 'react-native-paper';
import Navigation from './Routes';
const color = [
  '#FFCA28',
  '#4CAF50',
  'purple',
  'blue',
  '#B1E3E5',
  'green',
  '#2962FF',
  'grey',
  'orange',
  'jingga',
  '#6002EE',
];
const theme = {
  color: color,
  lightTheme,
};

const AppContainer = () => {
  return (
    <ApplicationProvider mapping={mapping} theme={theme}>
      <Provider>
        <Navigation />
      </Provider>
    </ApplicationProvider>
  );
};

export default AppContainer;
