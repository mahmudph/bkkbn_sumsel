import {VIDEO_URL_HOST, PDF_URL_HOST} from 'App/Services';

module.exports = [
  {
    data_id: 1,
    title: 'Konsep Dasar',
    key: 'BKB',
    deskripsi:
      'Mekanisme Pengelolaan Bina Keluarga Balita Holistik Integratif, Peningkatan kualitas SDM adalah pilar utama pembangunan, dimana sangat ditentukan oleh kualitas pembinaan keluarga sejak dini bahkan sejak janin dalam kandungan.',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/15.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/konsep-dasar.pdf?inline=false`,
  },
  {
    data_id: 2,
    title: null,
    deskripsi: null,
    key: 'BKB',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/1.png'),
    item: [
      {
        data_id: 1,
        judul: 'Kantong Wasiat Petemuan 1',
        key: 'BKB',
        title:
          'Perencanaan Hidup Berkeluarga dan Harapan Orang tua Terhadap Masa Depan Anak',
        deskripsi:
          '8 Fungsi Keluarga, keagamaan, sosial budaya, cinta kasih, perlindungan, reproduksi, sosialisasi dan pendidikan, ekonomi, pembinaan. ',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/1_1.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan1/1-kantong.pdf?inline=false`,
      },
      {
        data_id: 2,
        title: 'Kesiapan Menjadi Orang tua',
        key: 'BKB',
        deskripsi:
          'Merencanakan usia pernikahan, membina hubungan antar pasangan, merencakanan kelahirna anaka pertama, mengatur jarak kelahiran, berhenti mealahirkan di usia 35 tahun, merawat dan mengasuh anak usia balita.',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/1_2.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan1/1.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 3,
    title: null,
    deskripsi: null,
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/2.png'),
    item: [
      {
        data_id: 1,
        judul: 'Kantong Wasiat Petemuan 2',
        key: 'BKB',
        title: 'Memahami Konsep Diri yang Positif dan Konsep Pengasuhan',
        deskripsi:
          'Sebagai orang tua kita harus memiliki konsep diri yang positif sehingga dapat menerapkan pengasuhan yang baik, penuh kasih sayang dan berkualitas',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/2_1.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan2/2-kantong.pdf?inline=false`,
      },
      {
        data_id: 2,
        title: 'Memahami Konsep Diri Orang Tua',
        key: 'BKB',
        deskripsi:
          'Konsep Diri adalah gambaran diri seseirang tentang ciri-ciri yang dimilikinya, konsep diri berkembang sejak bayi sampai dewasa',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/2_2.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan2/2.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 4,
    title: null,
    deskripsi: null,
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/3.png'),
    item: [
      {
        data_id: 1,
        title: 'Keterlibatan Ayah Dalam Pengasuhan',
        key: 'BKB',
        deskripsi:
          'Keterlibatan ayah dalam pengasuhan sering hanya dianggap sebatas pendukung ibu, padahal ayah juga dapat melakukan pengasuhan yang sama baiknya dengan ibu.',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/3_2.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan3/3.1.pdf?inline=false`,
      },
      {
        data_id: 2,
        title: 'Peranan Orang Tua dalam Pengasuhan Balita',
        key: 'BKB',
        deskripsi:
          'Pengasuhan yang baik menghasilkan anak dengan kepribadian yang baik, sehingga menjadi orang dewasa yang cerdas, memiliki kemampuan berbicara yang baik.',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/3_3.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan3/3.2.pdf?inline=false`,
      },
      {
        data_id: 3,
        judul: 'Kantong Wasiat Petemuan 3',
        key: 'BKB',
        title: 'Peran Orang Tua dan Keterlibatan Ayah Dalam Pengasuhan',
        deskripsi:
          'Peran Ayah dan Ibu dalam mengasuh anak boleh berbeda tapi tanggung jawab dalam mengasuh sama besarnya.',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/3_1.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan3/3-kantong.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 5,
    title: null,
    deskripsi: null,
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/4.png'),
    item: [
      {
        data_id: 1,
        judul: 'Kantong Wasiat Petemuan 4',
        key: 'BKB',
        title: 'Menjaga Kesehatan Anak Usia Dini',
        deskripsi:
          'Orang tua harus menjaga kesehatan sejak masa kehamilan sejak anak usia dini supaya anak dapat tumbuh dan berkembang secara optimal.',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/4_1.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan4/4.pdf?inline=false`,
      },
      {
        data_id: 2,
        title: 'Menjaga Kesehatan Anak Usia Dini',
        key: 'BKB',
        deskripsi:
          'Biasakan hidup sehat, berikan anak makanan gizi yang seimbang, berikan anak imunisasi sedini mungkin.',
        imagename: require('App/Assets/image/bkb/4_2.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan4/4-kantong.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 6,
    title: 'Kantong Wasiat Pertemuan 5',
    key: 'BKB',
    deskripsi:
      'Makanan bergizi bagi anak usia dini harus diberikan sesuai dengan usia dan kebutuhannya, dari lahir hingga 6 bulan ASI saja sudah cukup bagi anak.',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/5.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/pertemuan_one_materi/5.pdf?inline=false`,
  },
  {
    data_id: 7,
    title: 'Kantong Wasiat Pertemuan 6',
    key: 'BKB',
    deskripsi:
      'Perilaku hidup bersih dan sehat(PHBS) pada anak usia dini dapat dibentuk dengan pembiasaan sehari-hari di rumah dan teladan dari orangtua.',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/6.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/pertemuan_one_materi/6.pdf?inline=false`,
  },
  {
    data_id: 8,
    title: 'Kantong Wasiat Pertemuan 7',
    key: 'BKB',
    deskripsi:
      'Anak bukanlah orang dewasa dalam bentuk mini sehingga memerlukan peran aktif orangtua agar dapat tumbuh dan berkembang secara optimal.',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/7.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/pertemuan_one_materi/7.pdf?inline=false`,
  },
  {
    data_id: 9,
    title: 'Kantong Wasiat Pertemuan 8',
    key: 'BKB',
    deskripsi:
      'Berkomunikasi bukan hanya merupakan rangkaian kata yang memiliki arti namun juga berupa ekspresi wajah dan bahasa tubuh.',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/8.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/pertemuan_one_materi/8.pdf?inline=false`,
  },
  {
    data_id: 10,
    title: 'Kantong Wasiat Pertemuan 9',
    key: 'BKB',
    deskripsi:
      'Anak akan mengalami kesulitan dalam bersosialisasi jika anak tidak diajarkan oleh oranguta tentang bagaiman caranya bergaul dam bertingkah laku yang sesuai dengan norma atau nilai yang dianut keluarga maupun masyarakat.',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/9.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/pertemuan_one_materi/9.pdf?inline=false`,
  },
  {
    data_id: 11,
    title: null,
    deskripsi: null,
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/10.png'),
    item: [
      {
        data_id: 1,
        judul: 'Kantong Wasiat Petemuan 10',
        key: 'BKB',
        title: 'Pengenalan Kesehatan Reproduksi Pada Anak Usia Dini',
        deskripsi:
          'Menjelaskan berbagai cara memberitahu kepada anak mengenai Kesehatan reproduksi',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/10_1.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan10/10-kantong.pdf?inline=false`,
      },
      {
        data_id: 2,
        title: 'Kesehatan Reproduksi Balita',
        key: 'BKB',
        deskripsi:
          'Orangtua tidak boleh menganggap tabu untuk membericakan tentang kesehatan reproduksi',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/10_2.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan10/10.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 12,
    title: 'Kantong Wasiat Pertemuan 11',
    key: 'BKB',
    deskripsi:
      'Ada 4 hak anak, hak hidup, hak tumbuh kembang, hak perlindungan dan hak partisipasi, hak anak dilindungi oleh Undang-Undang, orangtua berkewajiban memenuhi hak anak.',
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/11.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/pertemuan_one_materi/11.pdf?inline=false`,
  },
  {
    data_id: 13,
    title: null,
    deskripsi: null,
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/12.png'),
    item: [
      {
        data_id: 1,
        judul: 'Kantong Wasiat Petemuan 12',
        key: 'BKB',
        title: 'Menjaga Anak dari Pengaruh Media',
        deskripsi:
          'Untuk menjaga anak dari pengaruh media, orangtua perlu menyepakati kapan dan berapa lama media tersebut boleh digunakan oleh anak',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/12_1.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan12/12-kantong.pdf?inline=false`,
      },
      {
        data_id: 2,
        title: 'Menjaga Anak dari Pengaruh Media',
        key: 'BKB',
        deskripsi:
          'Untuk menjaga anak dari pengaruh media, orangtua perlu menyepakati kapan dan berapa lama media tersebut boleh digunakan oleh anak',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/12_2.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan12/12.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 14,
    title: null,
    deskripsi: null,
    // @ts-ignore
    imagename: require('App/Assets/image/bkb/13.png'),
    item: [
      {
        data_id: 1,
        key: 'BKB',
        judul: 'Kantong Wasiat Petemuan 13',
        title: 'Pembentukan Karakter Anak Sejak Dini',
        deskripsi:
          'Karakter yang baik adalah disiplin, jujur, mengetahui batas kemampuan diri sendiri dan menghargai orang lain ',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/13_1.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan13/13-kantong.pdf?inline=false`,
      },
      {
        data_id: 2,
        key: 'BKB',
        title: 'Pembentukan Karakter Anak Sejak Dini',
        deskripsi:
          'Karakter yang baik adalah disiplin, jujur, mengetahui batas kemampuan diri sendiri dan menghargai orang lain',
        // @ts-ignore
        imagename: require('App/Assets/image/bkb/13_2.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/pertemuan13/13.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 15,
    title: 'Kartu Kembang Anak',
    deskripsi: 'Ilustrasi mengenai Kartu Kembang yang terjadi pada anak',
    // @ts-ignore
    key: 'BKB',
    imagename: require('App/Assets/image/bkb/14.png'),
    ppt: `${PDF_URL_HOST}assets_bkb/kka.pdf?inline=false`,
    videoUrl: `${VIDEO_URL_HOST}assets_bkb/kka.mp4?inline=false`,
  },
  {
    data_id: 16,
    title: 'RR',

    deskripsi: 'Pencatatan & Pelaporan mengenai modul BKB',
    imagename: require('App/Assets/image/bkb/rr/pencatatan-laporan-bkb.png'),
    item: [
      {
        data_id: 1,
        key: 'BKB',
        title: 'Pencatatan & Pelaporan mengenai modul BKB',
        deskripsi:
          'Menjabarkan bagaimana cara melakukan pencatatan & pelaporan mengenai BKB',
        imagename: require('App/Assets/image/bkb/rr/pencatatan-laporan-bkb.png'),
        ppt: `${PDF_URL_HOST}assets_bkb/rr/Pencatatan%20dan%20Pelaporan%20BKB.pdf?inline=false`,
      },
      {
        data_id: 2,
        key: 'BKB',
        title: 'FORMULIR_DALLAP_2015_BKB-R1',
        deskripsi:
          'Register pembinaan ketahanan keluarga bina keluarga balita (BKB)',
        imagename: require('App/Assets/image/bkb/rr/pencatatan-laporan-bkb.png'),
        ppt:
          'https://github.com/mahmudph/assets_bkkbn/raw/master/FORMULIR_DALLAP_2015_BKB-R1.pdf',
      },
      {
        data_id: 3,
        key: 'BKB',
        title: 'FORMULIR_DALLAP_2015_BKB-KO',
        deskripsi:
          'Form pendaftaran kelompok kegiatan pembinaan ketahanan keluarga bina keluarga balita (BKB)',
        imagename: require('App/Assets/image/bkb/rr/pencatatan-laporan-bkb.png'),
        ppt:
          'https://github.com/mahmudph/assets_bkkbn/raw/master/FORMULIR_DALLAP_2015_BKB-KO.pdf',
      },
    ],
  },
];
