import {VIDEO_URL_HOST, PDF_URL_HOST} from 'App/Services';

module.exports = [
  {
    data_id: 1,
    title: 'Konsep Dasar BKL',
    imagename: require('App/Assets/image/bkl/konsep-dasar-kelompok.png'),
    item: [
      {
        data_id: 1,
        key: 'BKL',
        title: 'Konsep Dasar Lansia Tangguh',
        deskripsi:
          'Berisi materi dasar yang bertujuan meningkatakan kepedulian dan peran terhadap keluarga dalam mewujudkan lanjut usia yang tangguh (sehat, mandiri dan tangguh) ',
        imagename: require('App/Assets/image/bkl/konsep-dasar-kelompok.png'),
        ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/konsep-dasar.pdf`, //assets_bkl/materi_bkl/2.pdf?inline=false`,
        videoUrl: null,
      },
      {
        data_id: 2,
        key: 'BKL',
        title: 'Konsep Dasar Lansia Tangguh Video Eps 1',
        deskripsi:
          'Berisi materi dasar yang bertujuan meningkatakan kepedulian dan peran terhadap keluarga dalam mewujudkan lanjut usia yang tangguh (sehat, mandiri dan tangguh) ',
        imagename: require('App/Assets/image/bkl/konsep-dasar-kelompok.png'),
        ppt: null,
        videoUrl: `${VIDEO_URL_HOST}video_bkl/konsep-dasar/eps1.mp4?inline=false`,
        streamUrl: `${VIDEO_URL_HOST}compress-handbrake/eps1.m4v?inline=false`,
      },

      {
        data_id: 3,
        key: 'BKL',
        title: 'Konsep Dasar Lansia Tangguh Video Eps 2',
        deskripsi:
          'Berisi materi dasar yang bertujuan meningkatakan kepedulian dan peran terhadap keluarga dalam mewujudkan lanjut usia yang tangguh (sehat, mandiri dan tangguh) ',
        imagename: require('App/Assets/image/bkl/konsep-dasar-kelompok.png'),
        videoUrl: `${VIDEO_URL_HOST}video_bkl/konsep-dasar/eps2.mp4?inline=false`,
        streamUrl: `${VIDEO_URL_HOST}compress-handbrake/eps2.m4v?inline=false`,
      },
      {
        data_id: 4,
        key: 'BKL',
        title: 'Konsep Dasar Lansia Tangguh Video Eps 3',
        deskripsi:
          'Berisi materi dasar yang bertujuan meningkatakan kepedulian dan peran terhadap keluarga dalam mewujudkan lanjut usia yang tangguh (sehat, mandiri dan tangguh) ',
        imagename: require('App/Assets/image/bkl/konsep-dasar-kelompok.png'),
        videoUrl: `${VIDEO_URL_HOST}video_bkl/konsep-dasar/eps3.mp4?inline=false`,
        streamUrl: `${VIDEO_URL_HOST}compress-handbrake/eps3.m4v?inline=false`,
      },
    ],
  },
  {
    data_id: 2,
    key: 'BKL',
    title: 'Kebijakan Pembangunan Keluarga Lansia Tangguh',
    deskripsi:
      'Berisi materi pengertian lansia, kondisi pendudukan indonesia saat ini serta pembangunan lansia tangguh',
    imagename: require('App/Assets/image/bkl/1.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/1.pdf?inline=false`,
  },
  {
    data_id: 3,
    title: 'Konsep Dasar Lansia Tangguh',
    key: 'BKL',
    deskripsi:
      'Berisi materi pengertian lansia, kondisi pendudukan indonesia saat ini serta pembangunan lansia tangguh',
    imagename: require('App/Assets/image/bkl/2.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/1.pdf?inline=false`,
    videoUrl: `${VIDEO_URL_HOST}video_bkl/pertemuan2/1.mp4?inline=false`,
    streamUrl: `${VIDEO_URL_HOST}compress-handbrake/pertemuan2.m4v?inline=false`,
  },
  {
    data_id: 4,
    title: 'Spritual',
    key: 'BKL',
    deskripsi:
      'Materi berisi berupa pembahasan mengenai perubahan spritual yang dialami lansia',
    imagename: require('App/Assets/image/bkl/3.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/3.pdf?inline=false`,
    videoUrl: `${VIDEO_URL_HOST}video_bkl/pertemuan3/1.mp4?inline=false`,
    streamUrl: `${VIDEO_URL_HOST}compress-handbrake/pertemuan3.m4v?inline=false`,
  },
  {
    data_id: 5,
    title: 'Intelektual',
    key: 'BKL',
    deskripsi: 'Materi Intelektual',
    imagename: require('App/Assets/image/bkl/4.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/4.pdf?inline=false`,
    videoUrl: `${VIDEO_URL_HOST}video_bkl/pertemuan4/1.mp4?inline=false`,
    streamUrl: `${VIDEO_URL_HOST}compress-handbrake/pertemuan4.m4v?inline=false`,
  },
  {
    data_id: 6,
    key: 'BKL',
    title: 'Fisik',
    deskripsi:
      'Materi berisi pemahaman dalam Perubahan fisik yang terjadi pada lansia',
    imagename: require('App/Assets/image/bkl/5.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/5.pdf?inline=false`,
    videoUrl: null,
  },
  {
    data_id: 7,
    key: 'BKL',
    title: 'Video Dimensi Fisik',
    deskripsi: 'Video Dimensi Fisik',
    imagename: require('App/Assets/image/bkl/6.png'),
    ppt: null,
    videoUrl:
      'https://github.com/mahmudph/assets_bkkbn/raw/master/dimensi_fisik.mp4',
  },
  {
    data_id: 8,
    key: 'BKL',
    title: 'Kesehatan Reproduksi Lansia',
    deskripsi:
      'Pemahaman mengenai hal yang perlu diperhatikan terhadap kesehatan reproduksi lansia seperti menopouse, andropouse dan lain lain',
    imagename: require('App/Assets/image/bkl/7.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/7.pdf?inline=false`,
    videoUrl: null,
  },
  {
    data_id: 9,
    title: 'Emosional',
    key: 'BKL',
    deskripsi:
      'Pemahaman mengenai konsep emosional, gejala dan masalah psikologis lansia, cara berkomunikasi dengan lansia dan lain-lain',
    imagename: require('App/Assets/image/bkl/8.png'),
    ppt: `${PDF_URL_HOST}/assets_bkl/materi_bkl/8.pdf?inline=false`,
    videoUrl: null,
  },
  {
    data_id: 10,
    title: 'Video Dimensi Emosional',
    key: 'BKL',
    deskripsi:
      'Dimensi Emosional, Media Poster Seri Terbuka Lansia Mantap dan Berdaya',
    imagename: require('App/Assets/image/bkl/9.png'),
    ppt: null,
    videoUrl: `${VIDEO_URL_HOST}video_bkl/pertemuan9/1.mp4?inline=false`,
    streamUrl: `${VIDEO_URL_HOST}compress-handbrake/pertemuan9.m4v?inline=false`,
  },
  {
    data_id: 11,
    title: 'Sosial Kemasyarakatan',
    key: 'BKL',
    deskripsi:
      'Materi beirisi pemahaman lansia dalam bersosialisasi antar lansia',
    imagename: require('App/Assets/image/bkl/10.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/10.pdf?inline=false`,
    videoUrl: `${VIDEO_URL_HOST}video_bkl/pertemuan10/1.mp4?inline=false`,
    streamUrl: `${VIDEO_URL_HOST}compress-handbrake/pertemuan10.m4v?inline=false`,
  },
  {
    data_id: 12,
    title: 'Vokasional',
    key: 'BKL',
    deskripsi:
      'Menjelaskan potensi-potensi yang dimiliki lansia, pengembangan profesional vokasional lansia, cara mengelola uang hingga usaha ekonomi lansia ',
    imagename: require('App/Assets/image/bkl/11.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/11.pdf?inline=false`,
    videoUrl: `${VIDEO_URL_HOST}video_bkl/pertemuan11/VTS_05_1 DIMENSI PROFESIONAL VOKASIONAL.mp4.mp4?inline=false`,
    streamUrl: `${VIDEO_URL_HOST}compress-handbrake/pertemuan11.m4v?inline=false`,
  },
  {
    data_id: 13,
    key: 'BKL',
    title: 'Praktek Vokasional',
    deskripsi:
      'Menjelaskan potensi-potensi yang dimiliki lansia, pengembangan profesional vokasional lansia, cara mengelola uang hingga usaha ekonomi lansia ',
    imagename: require('App/Assets/image/bkl/12.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/12.pdf?inline=false`,
    videoUrl: null,
  },
  {
    data_id: 14,
    title: 'Lingkungan',
    key: 'BKL',
    deskripsi:
      'Pemahaman dalam segi lingkungan terhadap pembangunan keluarga lansia tangguh',
    imagename: require('App/Assets/image/bkl/13.png'),
    ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/13.pdf?inline=false`,
    videoUrl: `${VIDEO_URL_HOST}video_bkl/pertemuan13/VTS_07_1.mp4?inline=false`,
    streamUrl: `${VIDEO_URL_HOST}compress-handbrake/pertemuan13.m4v?inline=false`,
  },
  {
    data_id: 15,
    title: 'RR BKL',
    imagename: require('App/Assets/image/bkl/rr.png'),
    item: [
      {
        data_id: 1,
        key: 'BKL',
        title: 'Formulir Dallap 2015 BKL KO',
        deskripsi:
          'Form pendaftaran kelompok kegiatan pembinaan ketahanan keluarga bina keluarga lansia (BKL)',
        imagename: require('App/Assets/image/bkl/rr.png'),
        ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/pencatatan_bkl/FORMULIR_DALLAP_2015_BKL-KO.pdf?inline=false`,
      },
      {
        data_id: 2,
        key: 'BKL',
        title: 'Formulir Dallap 2015 BKL R1',
        deskripsi:
          'Register pembinaan ketahanan keluarga bina keluarga lansia (BKL)',
        imagename: require('App/Assets/image/bkl/rr.png'),
        ppt: `${PDF_URL_HOST}assets_bkl/materi_bkl/pencatatan_bkl/FORMULIR_DALLAP_2015_BKL-R1.pdf?inline=false`,
      },
    ],
  },
];
