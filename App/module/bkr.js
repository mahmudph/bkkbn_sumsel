import {VIDEO_URL_HOST, PDF_URL_HOST} from 'App/Services';

module.exports = [
  {
    data_id: 1,
    key: 'BKR',
    title: 'Konsep Dasar',
    deskripsi:
      'Meningkatan pengetahuan dan keterampilan orangtua dan anggota keluarga lainnya dalam pengasuhan dan pembinaan tumbuh kembang remaja.',
    imagename: require('App/Assets/image/bkr/13.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/konsep-dasar.pdf?inline=false`,
  },
  {
    data_id: 2,
    key: 'BKR',
    title: 'Pendewasaan Usia Perkawinan',
    deskripsi: 'Konsep Perkawinan, PUP dan Kematangan psikologis remaja',
    imagename: require('App/Assets/image/bkr/2.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/1.pdf?inline=false`,
  },
  {
    data_id: 3,
    key: 'BKR',
    title: '8 Fungsi Keluarga',
    deskripsi:
      'Fungsi Keagamaan, Sosial Budaya, Cinta dan Kasih Sayang, Perlindungan, Reproduksi, Sosialisasi dan Pendidikan, Ekonomi, Lingkungan.',
    imagename: require('App/Assets/image/bkr/2.png'),
    ppt: `${PDF_URL_HOST}Sassets_bkr/materi_bkr/2.pdf?inline=false`,
  },
  {
    data_id: 4,
    key: 'BKR',
    title: 'NKKBS',
    deskripsi:
      'Keluarga adalah rumah tangga yang memiliki hubungan darah atau perkawinan atau menyediakan terselenggaranya fungsi-fungsi instrumental mendasar dan ekspresif.',
    imagename: require('App/Assets/image/bkr/3.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/3.pdf?inline=false`,
  },
  {
    data_id: 5,
    key: 'BKR',
    title: 'Nilai Gender Dalam Keluarga',
    deskripsi:
      'Kesetaraan dan keadilan gender harus dilandasi mindset bahwa perempuan dan laki-laki adalah aset dan potensi pembangunan yang akan mempengaruhi arah pembangunan nasional (KPPPA,2010)',
    imagename: require('App/Assets/image/bkr/4.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/4.pdf?inline=false`,
  },
  {
    data_id: 6,
    key: 'BKR',
    title: 'Seksualitas',
    deskripsi:
      'Semua yang berhubungan dengan manusia sebagai makhluk sosial, yaitu emosi, perasaan, kepribadian dan sikap yang berkaitan dengan perilaku seksual hubungan seksual dan orientasi seksual.',
    imagename: require('App/Assets/image/bkr/5.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/5.pdf?inline=false`,
  },
  {
    data_id: 7,
    key: 'BKR',
    title: 'NAPZA',
    deskripsi:
      'Napza merupakan singkatan dari Narkoba Alkohol Psikotropika Zat adiktif',
    imagename: require('App/Assets/image/bkr/6.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/6.pdf?inline=false`,
  },
  {
    data_id: 8,
    key: 'BKR',
    title: 'HIV & AIDS',
    deskripsi:
      'HIV & Aids merupakan virus yang menurunkan sistem kekebalan tubuh. ',
    imagename: require('App/Assets/image/bkr/7.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/7.pdf?inline=false`,
  },
  {
    data_id: 9,
    key: 'BKR',
    title: 'Komunikasi Orang Tua dengan Remaja',
    deskripsi:
      'Komunikasi adalah suatu proses penyampaian pikiran dan perasaan melalui bahasa, pembicaraan, mendengar, gerak tubuh atau ungkapan emosi.',
    imagename: require('App/Assets/image/bkr/8.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/8.pdf?inline=false`,
  },
  {
    data_id: 10,
    key: 'BKR',
    title: 'Peran Orang Tua Dalam Pembinaan Remaja',
    deskripsi: 'Orangtua adalah "sekolah" pertama bagi kehidupan anak.',
    imagename: require('App/Assets/image/bkr/9.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/9.pdf?inline=false`,
  },
  {
    data_id: 11,
    key: 'BKR',
    title: 'Kebersihan Dan Kesehatan Tubuh',
    deskripsi:
      'Hal penting yang perlu di perhatikan dan dijaga bagi remaja antara lain; rambut, jerawat, keringat, kulit, merawat gigi, dlll.',
    imagename: require('App/Assets/image/bkr/10.png'),
    ppt: `${PDF_URL_HOST}/assets_bkr/materi_bkr/10.pdf?inline=false`,
  },
  {
    data_id: 12,
    key: 'BKR',
    title: 'Pemenuhan Gizi Remaja',
    deskripsi:
      'Gizi adalah elemen yang terdapat dalam makanan dan dapat dimanfaatkan secara langsung oleh tubuh seperti karbohidrat, protein, lemak, dll.',
    imagename: require('App/Assets/image/bkr/11.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/11.pdf?inline=false`,
  },
  {
    data_id: 13,
    key: 'BKR',
    title: 'Menjaga Anak Dari Pengaruh Media',
    deskripsi:
      'Memahami perilaku anak di era digital beserta dampaknya dan memahami apa yang bsia dilakukan bersama anak.',
    imagename: require('App/Assets/image/bkr/12.png'),
    ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/12.pdf?inline=false`,
  },
  {
    data_id: 14,
    title: 'Pencatatan BKR',
    deskripsi: ' ',
    imagename: require('App/Assets/image/bkr/14.png'),
    item: [
      {
        data_id: 1,
        key: 'BKR',
        title: 'Formulir DALLAP 2015 BKR-KO',
        deskripsi:
          'Form pendaftaran kelompok kegiatan pembinaan ketahanan keluarga bina keluarga remaja (BKR)',
        // @ts-ignore
        imagename: require('App/Assets/image/bkr/14.png'),
        ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/pencatatan_bkr/FORMULIR_DALLAP_2015_BKR-KO.pdf?inline=false`,
      },
      {
        data_id: 2,
        key: 'BKR',
        title: 'Formulir DALLAP 2015 BKR-RI',
        deskripsi:
          'Register pembinaan ketahanan keluarga bina keluarga remaja (BKR)',
        imagename: require('App/Assets/image/bkr/14.png'),
        ppt: `${PDF_URL_HOST}assets_bkr/materi_bkr/pencatatan_bkr/FORMULIR_DALLAP_2015_BKR-RI.pdf?inline=false`,
      },
    ],
  },
  {
    data_id: 15,
    key: 'BKR',
    title: 'Video Bina Keluarga Remaja',
    deskripsi:
      'Vidio dari permainan gendre kids dari PIK-R Perabangsa yang berisi pembelajaran terhadap anak yang menginjak remaja',
    imagename: require('App/Assets/image/bkr/15.png'),
    videoUrl: `${VIDEO_URL_HOST}video_bkr/Monopoli_Genre%20Kit%20PIK%20R%20Perabangsa.mp4?inline=false'`,
  },
];
