/*
 * all data stored here
 */

import assets from 'App/Assets';

module.exports = {
  data: [
    {
      id: 1,
      key: 'BKB',
      img: assets.cover.satu,
      description:
        'Membahas mengenai modul yang berkaitan dengan segala aspek yang berkaitan dengan balita ',
      data: require('./bkb'),
    },
    {
      id: 2,
      key: 'BKR',
      img: assets.cover.tiga,
      description:
        'Membabahas mengenai modul yang berkaitan segala aspek yang berkatian dengan tumbuh kembang remaja',
      data: require('./bkr'),
    },
    {
      id: 3,
      key: 'BKL',
      description:
        'Membahas mengenai modul yang berkaitan dengann penanganan terhadap orang tua atau lansia',
      img: assets.cover.dua,
      data: require('./bkl'),
    },
  ],
};
